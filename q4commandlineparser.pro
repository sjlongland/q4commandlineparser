lessThan(QT_MAJOR_VERSION, 5) {
    TEMPLATE = subdirs
    SUBDIRS = src tests
    CONFIG += ordered

    !infile($$OUT_PWD/.qmake.cache, Q4COMMANDLINEPARSER_PROJECT_ROOT) {
        system("echo Q4COMMANDLINEPARSER_PROJECT_ROOT = $$PWD >> $$OUT_PWD/.qmake.cache")
        system("echo Q4COMMANDLINEPARSER_BUILD_ROOT = $$OUT_PWD >> $$OUT_PWD/.qmake.cache")
    }
} else {
    load(qt_parts)
}
