INCLUDEPATH += $$PWD

 PUBLIC_HEADERS += \
    $$PWD/qcommandlineparserglobal.h \
    $$PWD/qcommandlineparser.h \
    $$PWD/qcommandlineoption.h

SOURCES += \
    $$PWD/qcommandlineparser.cpp \
    $$PWD/qcommandlineoption.cpp

symbian {
    MMP_RULES += EXPORTUNFROZEN
    #MMP_RULES += DEBUGGABLE_UDEBONLY
    TARGET.UID3 = 0xE7E62DFD
    TARGET.CAPABILITY =
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = Q4CommandLineParser.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles

    # FIXME !!!
    #INCLUDEPATH += c:/Nokia/devices/Nokia_Symbian3_SDK_v1.0/epoc32/include/platform
    INCLUDEPATH += c:/QtSDK/Symbian/SDKs/Symbian3Qt473/epoc32/include/platform    
}

HEADERS += $$PUBLIC_HEADERS
