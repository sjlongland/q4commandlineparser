CONFIG += testcase parallel_test
TARGET = tst_qcommandlineparser
QT = core testlib
SOURCES = tst_qcommandlineparser.cpp
CONFIG -= app_bundle

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += commandlineparser
} else {
    include($$Q4COMMANDLINEPARSER_PROJECT_ROOT/src/commandlineparser/qt4support/commandlineparser.prf)
}
